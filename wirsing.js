function registerListener(){
    console.log("Load");
    document.addEventListener("click", placeWirsing, false);
}

function placeWirsing(clickEvent){
    var newWirsing = document.createElement("wirsing");
    var wirsingImage = document.createElement("img");
    wirsingImage.setAttribute("src", "wirsing.png");
    wirsingImage.setAttribute("class", "wirsing");
    newWirsing.appendChild(wirsingImage);
    newWirsing.style.position = "absolute";
    newWirsing.style.left = (clickEvent.x - 70)+'px';
    newWirsing.style.top = (clickEvent.y - 70)+'px';
    document.body.appendChild(newWirsing);
    playWirsingAudio();
}

function playWirsingAudio(){
    //Currently we have 3 files not pretty but whatever hardcoded magic numbers rock
    var randomFile = Math.floor(Math.random() * 3) + 1;
    var wirsingWav = "wirsingtts" + randomFile + ".wav";
    console.log(wirsingWav);
    var audio = new Audio(wirsingWav);
    audio.play();
}